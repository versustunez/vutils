package dev.vstz.utils

import org.json.simple.JSONValue
import java.lang.StringBuilder

object JSON {
    fun build(builder: StringBuilder): String {
        return JSONValue.escape(builder.toString())
    }

    fun buildObject(builder: StringBuilder, hasNext: Boolean): StringBuilder {
        val jsonObject = StringBuilder("{")
        jsonObject.append(builder).append("}")
        if (hasNext) {
            jsonObject.append(",")
        }
        return jsonObject
    }

    fun buildArray(builder: StringBuilder, hasNext: Boolean): StringBuilder {
        val jsonObject = StringBuilder("[")
        jsonObject.append(builder).append("]")
        if (hasNext) {
            jsonObject.append(",")
        }
        return jsonObject
    }

    fun buildEntry(title: String, value: String, hasNext: Boolean, builder: StringBuilder) {
        builder.append(quote(title)).append(":").append(quote(JSONValue.escape(value)))
        if (hasNext) {
            builder.append(",")
        }
    }

    fun buildArrayEntry(value : String, hasNext: Boolean, builder: StringBuilder) {
        builder.append(quote(JSONValue.escape(value)))
        if (hasNext) {
            builder.append(",")
        }
    }

    fun buildEntry(title: String, value: StringBuilder, hasNext: Boolean, builder: StringBuilder) {
        builder.append(quote(title)).append(":").append(value)
        if (hasNext) {
            builder.append(",")
        }
    }

    fun quote(value: String): String {
        return "\"" + value + "\""
    }
}