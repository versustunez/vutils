package dev.vstz.utils

import dev.vstz.utils.out.Console
import java.io.*
import java.nio.charset.StandardCharsets
import java.util.stream.Collectors

object FileReader {
    fun readFileContent(file: File?, log: Boolean = true): String {
        val contentBuilder = StringBuilder()
        if (file == null) {
            return ""
        }
        try {
            contentBuilder.append(file.readText(StandardCharsets.UTF_8))
        } catch (e: IOException) {
            if (log) {
                Console.exception(e)
            }
        }
        return contentBuilder.toString()
    }

    fun readContentFromStream(stream: InputStream): String {
        try {
            val reader = BufferedReader(InputStreamReader(stream))
            return reader.lines().collect(Collectors.joining("\n"))
        } catch (e: Exception) {
            Console.exception(e.toString())
        }
        return ""
    }
}