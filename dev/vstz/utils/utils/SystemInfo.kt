package dev.vstz.utils

import java.text.NumberFormat

object SystemInfo {

    private val runtime = Runtime.getRuntime()
    fun memInfoForJSON(): java.lang.StringBuilder {
        val format = NumberFormat.getInstance()
        val sb = StringBuilder()
        val maxMemory = runtime.maxMemory()
        val allocatedMemory = runtime.totalMemory()
        val freeMemory = runtime.freeMemory()
        JSON.buildEntry("used", format.format((allocatedMemory - freeMemory) / 1024).replace(".", ""), true, sb)
        JSON.buildEntry("free", format.format(freeMemory / 1024).replace(".", ""), true, sb)
        JSON.buildEntry("allocated", format.format(allocatedMemory / 1024).replace(".", ""), true, sb)
        JSON.buildEntry("max", format.format(maxMemory / 1024).replace(".", ""), true, sb)
        JSON.buildEntry("total_free", format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024).replace(".", ""), false, sb)
        return JSON.buildObject(sb, false)
    }
}