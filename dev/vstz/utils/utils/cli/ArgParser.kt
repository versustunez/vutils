package dev.vstz.utils.cli

import dev.vstz.utils.FileReader
import dev.vstz.utils.out.ANSI
import dev.vstz.utils.out.Console
import java.lang.Exception
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.system.exitProcess

/**
 * Object to read the current Arguments and build a HashMap for it... if Args is empty show a help site with all available arguments...
 * List of Arguments is in the Resource folder...
 */
object ArgParser {
    private val arguments: HashMap<String, String> = HashMap()
    private val availableArgs: TreeMap<String, Argument> = TreeMap()
    var invalidArgs = 0
    private var hasRequiredArgument = false

    private fun reset() {
        arguments.clear()
        availableArgs.clear()
        hasRequiredArgument = false
        invalidArgs = 0
    }

    fun init(name: String?) {
        reset()
        val fileName = name ?: "/arguments.txt"
        val file = ArgParser::class.java.classLoader.getResource(fileName)?.openStream()
        if (file == null) {
            Console.exception("Argument file not found.")
            return
        }
        loadAvailableArguments(
            FileReader.readContentFromStream(
                file
            )
        )
    }

    fun getOrDefault(name: String, default: String): String {
        return arguments.getOrDefault(name, default)
    }

    private fun loadAvailableArguments(fileContent: String) {
        if (fileContent == "") {
            return
        }
        val lines = fileContent.split("\n")
        for (line in lines) {
            if (line == "" || line.startsWith("#")) {
                continue
            }
            try {
                val c = line.split(" | ")
                val isRequired = c[1] == "true"
                availableArgs[c[0]] = Argument(c[0], isRequired, c[2] == "true", c[3], c[4])
                if (isRequired) {
                    hasRequiredArgument = true
                }
            } catch (e: Exception) {
                Console.exception("Argument: \"$line\" is invalid...")
                invalidArgs++
                continue
            }
        }
    }

    fun validateArgs(args: Array<String>): Boolean {
        if (args.isEmpty() && hasRequiredArgument) {
            Console.raw(showHelp())
            return false
        } else {
            if (args.isNotEmpty() && (args[0] == "--help" || args[0] == "-h")) {
                Console.raw(showHelp())
                exitProcess(1)
            }
            for (i in args.indices) {
                if (!args[i].startsWith("--")) {
                    continue
                }
                val arg = args[i].trim().replace("--", "")
                val select = availableArgs[arg]
                if (select == null) {
                    Console.exception("Argument not in list: $arg")
                    Console.raw(showHelp())
                    return false
                }
                //if need value check if next element is there
                if (select.needValue) {
                    if (i + 1 < args.size && !args[i + 1].startsWith("--")) {
                        val value = args[i + 1]
                        arguments[arg] = value
                    } else {
                        Console.exception("No Value provided for \"${select.name}\"")
                        return false
                    }
                } else {
                    arguments[arg] = select.default
                }
            }
        }

        val builder = StringBuilder()
        var found = false
        for (arg in availableArgs.values) {
            if (arg.required) {
                if (arguments[arg.name] == null) {
                    if (found) {
                        builder.append(", ")
                    }
                    builder.append(arg.name)
                    found = true
                }
            }
        }
        if (found) {
            Console.exception("required Argument(s) not provided: $builder")
            Console.raw(showHelp())
            return false
        }
        return true
    }

    private fun showHelp(): String {
        val builder = StringBuilder("")
        var previous = false
        var isFirst = true
        val sorted = ArrayList<Argument>()
        for (arg in availableArgs.values) {
            sorted.add(arg)
        }
        sorted.sortBy { !it.required }
        for (arg in sorted) {
            if (previous != arg.required) {
                if (!isFirst) {
                    builder.append("\n")
                }
                builder.append("${ANSI.GREEN}${ANSI.HIGH_INTENSITY}<--- ")
                if (arg.required) {
                    builder.append("REQUIRED")
                } else {
                    builder.append("OPTIONAL")
                }
                builder.append(" --->${ANSI.SANE}\n")
                previous = arg.required
                isFirst = false
            }
            builder.append("${ANSI.BLUE}--${arg.name} ${ANSI.SANE}= ${arg.help} \n")
        }
        return builder.toString()
    }
}