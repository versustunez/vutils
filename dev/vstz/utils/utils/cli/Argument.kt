package dev.vstz.utils.cli

data class Argument constructor(
    val name: String,
    val required: Boolean,
    val needValue: Boolean,
    val help: String,
    val default: String
)