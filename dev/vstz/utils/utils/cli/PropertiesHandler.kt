package dev.vstz.utils.cli

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.*

class PropertiesHandler constructor(val file: String) {
    val properties = Properties()
    fun readPropertiesFile() {
        checkExists()
        val inputStream = FileInputStream(file)
        properties.load(inputStream)
    }

    fun savePropertiesFile() {
        checkExists()
        val fileOutputStream = FileOutputStream(file)
        properties.store(fileOutputStream, "save to properties file")
    }

    fun createFromHashMap(map: HashMap<String, String>) {
        properties.putAll(map)
    }

    fun checkExists(): Boolean {
        val fileObj = File(file)
        if (fileObj.isFile) {
            return true
        }
        fileObj.parentFile.mkdirs()
        fileObj.createNewFile()
        return false
    }
}