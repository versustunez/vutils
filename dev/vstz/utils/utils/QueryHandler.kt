package dev.vstz.utils

import java.net.URLDecoder
import java.nio.charset.StandardCharsets

//make multiple get's to an array! smooth<3
object QueryHandler {

    fun parseGET(requestedQuery: String): HashMap<String, ArrayList<String>> {
        return parseQuery(requestedQuery)
    }

    fun parseURI(urlParameters: String): ArrayList<String> {
        val map: ArrayList<String> = ArrayList()
        val paths = urlParameters.split("/")
        for (i in paths.indices) {
            val path = paths[i]
            if (path == "") {
                continue
            }
            map.add(path)
        }
        return map
    }

    private fun parseQuery(query: String?): HashMap<String, ArrayList<String>> {
        val params = HashMap<String, ArrayList<String>>()
        if (query == null) {
            return params
        }
        val pairs = query.split("[&]".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (pair in pairs) {
            val param = pair.split("[=]".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (param.size != 2) {
                continue
            }
            val key = URLDecoder.decode(param[0], StandardCharsets.UTF_8.toString())
            val value = URLDecoder.decode(param[1], StandardCharsets.UTF_8.toString())

            // if params already has the key than skip it...
            if (!params.containsKey(key)) {
                params[key] = ArrayList()
            }
            params[key]?.add(value)
        }
        return params
    }
}
