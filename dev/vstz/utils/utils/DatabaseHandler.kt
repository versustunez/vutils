package dev.vstz.utils

import dev.vstz.utils.out.ANSI
import dev.vstz.utils.out.Console
import dev.vstz.utils.out.Monitor
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLException
import java.text.MessageFormat
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set
import kotlin.system.exitProcess

class DatabaseHandler constructor(private val connectionString: String) {
    private var connection: Connection? = null

    init {
        this.initDatabase()
    }

    private fun initDatabase() {
        try {
            connection = DriverManager.getConnection(connectionString)
        } catch (e: SQLException) {
            Console.exception("Cannot connect to Database: ${e.message}")
            exitProcess(ErrorCodes.FAILED_TO_CONNECT_TO_SQL)
        }

    }

    fun getOne(statement: String, args: Map<String, String>?): Map<String, String>? {
        val r = get(statement, args, 1)
        return if (r != null && r.isNotEmpty()) {
            r[0]
        } else null
    }

    fun getLimited(statement: String, args: Map<String, String>?, limit: Int): List<Map<String, String>>? {
        return get(statement, args, limit)
    }

    fun getAll(statement: String, args: Map<String, String>?): List<Map<String, String>>? {
        return get(statement, args, 0)
    }

    fun getAsStream(stmt: String, args: Map<String, String>?): ResultSet? {
        var query = stmt
        if (args != null) {
            query = getPreparedStatement(query, args)
        }

        try {
            if (connection !!.isClosed) {
                connection = null
                System.gc()
                initDatabase()
            }
            val statement =
                connection !!.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)
            statement.maxRows = 0
            statement.fetchSize = Integer.MIN_VALUE
            statement.setEscapeProcessing(true)
            statement.executeQuery()
            statement.closeOnCompletion()
            return statement.resultSet
        } catch (e: Exception) {
            Console.exception(e.message.toString())
        }
        return null
    }


    private operator fun get(stmt: String, args: Map<String, String>?, limit: Int): List<Map<String, String>>? {
        val mon = Monitor("SQL Query: ${ANSI::GREEN.get()}$stmt${ANSI::SANE.get()}")
        mon.debugOnly = true
        var query = stmt
        if (args != null) {
            query = getPreparedStatement(query, args)
        }

        try {
            if (connection !!.isClosed) {
                connection = null
                System.gc()
                initDatabase()
            }
            val statement = connection !!.createStatement()
            statement.maxRows = limit
            statement.setEscapeProcessing(true)
            statement.execute(query)
            val result = statement.resultSet
            val assoc = toAssoc(result)
            statement.close()
            result.close()
            mon.finish()
            return assoc
        } catch (e: Exception) {
            Console.exception(e.message.toString())
        }
        mon.finish()
        return null
    }

    private fun toAssoc(set: ResultSet): List<Map<String, String>>? {
        val wantedColumnNames: List<String>
        return try {
            wantedColumnNames = getColumnNames(set)
            toList(set, wantedColumnNames)
        } catch (e: SQLException) {
            Console.exception(e.message.toString())
            null
        }

    }

    @Throws(SQLException::class)
    private fun toList(rs: ResultSet, wantedColumnNames: List<String>): List<Map<String, String>> {
        val rows = ArrayList<Map<String, String>>()

        while (rs.next()) {
            rows.add(toItem(rs, wantedColumnNames))
        }

        return rows
    }

    fun toItem(rs: ResultSet, wantedColumnNames: List<String>): HashMap<String, String> {
        val row = HashMap<String, String>()
        for (columnName in wantedColumnNames) {
            var value: Any? = rs.getString(columnName)
            if (value == null) {
                value = ""
            }
            row[columnName] = value.toString()
        }
        return row
    }

    @Throws(SQLException::class)
    fun getColumnNames(rs: ResultSet): List<String> {
        val columnNames = ArrayList<String>()

        val meta = rs.metaData

        val numColumns = meta.columnCount
        for (i in 1 .. numColumns) {
            columnNames.add(meta.getColumnName(i))
        }

        return columnNames
    }

    private fun getPreparedStatement(stmt: String, args: Map<String, String>): String {
        var statement = stmt
        for (key in args.keys) {
            var arg: String? = args[key]
            if (arg == null) {
                arg = "NULL"
            }
            arg = arg.replace("[(\"<;>')]".toRegex(), "")
            statement = statement.replace(key.toRegex(), arg)
        }
        return statement
    }

    fun execute(stmt: String, args: Map<String, String>?): Boolean {
        val mon = Monitor("SQL Query: ${ANSI::GREEN.get()}$stmt${ANSI::SANE.get()}")
        mon.debugOnly = true
        var query = stmt
        if (args != null) {
            query = getPreparedStatement(query, args)
        }

        return try {
            if (connection !!.isClosed) {
                connection = null
                System.gc()
                initDatabase()
            }
            val statement = connection !!.createStatement()
            statement.maxRows = 0
            statement.setEscapeProcessing(true)
            statement.execute(query)
            true
        } catch (e: Exception) {
            Console.exception(e.message.toString())
            false
        } finally {
            mon.finish()
        }
    }

    companion object {
        fun createWithArgs(args: Array<String>): DatabaseHandler {
            return DatabaseHandler(MessageFormat.format(
                    "jdbc:mysql://{0}:{1}/{4}?user={2}&password={3}&useServerPrepStmts=true&useCursorFetch=true&autoReconnect=true",
                    *args))
        }
    }
}
