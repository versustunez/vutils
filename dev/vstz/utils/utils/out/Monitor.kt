package dev.vstz.utils.out

class Monitor constructor(title: String) {
    private var title: String = ""
    private val MODULE = "Monitor"
    private var startTime: Long
    var debugOnly: Boolean = false
    var warnOnly: Boolean = false

    init {
        this.title = "\"" + title + "\""
        startTime = System.nanoTime()
    }

    fun finish() {
        val endTime = System.nanoTime()
        val time = endTime - startTime
        val parsed = time / 1000000
        val st = "$title took ~$parsed ms"
        if (debugOnly) {
            Console.debug(st, MODULE)
        } else {
            if (parsed > 2000) {
                Console.warn(st, MODULE)
            } else {
                if (! warnOnly) {
                    Console.log(st, MODULE)
                }
            }
        }
    }

}