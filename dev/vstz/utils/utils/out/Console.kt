package dev.vstz.utils.out

object Console {
    private val MODULE = "ConsoleManager"

    //private static boolean useAnsi = System.console() != null && System.getenv().get("TERM") != null;
    private val useAnsi = true
    var testMode = false

    var USEDEBUG = false
    var LOG = 1
    var WARN = 2
    var ERROR = 3
    var DEBUG = 4

    fun toggleDebugMode() {
        USEDEBUG = ! USEDEBUG
        log("Debug Mode: $USEDEBUG", MODULE)
    }

    fun toggleDebugMode(mode: Boolean) {
        USEDEBUG = mode
        log("Debug Mode: $USEDEBUG", MODULE)
    }

    // used for normal logging
    fun warn(value: Any) {
        log(value, "")
    }

    fun warn(value: Any, module: String) {
        printMessage(module, value, WARN)
    }

    @JvmOverloads
    fun log(value: Any, module: String = "") {
        printMessage(module, value, LOG)
    }

    fun log(vararg args: Any) {
        log(args, "")
    }

    fun error(vararg args: Any) {
        error(args, "")
    }

    fun warn(vararg args: Any) {
        warn(args, "")
    }

    fun debug(vararg args: Any) {
        debug(args, "")
    }

    @JvmOverloads
    fun error(value: Any, module: String = "") {
        printMessage(module, value, ERROR)
    }

    fun debug(value: Any) {
        debug(value.toString(), "")
    }

    fun debug(value: Any, module: String) {
        if (USEDEBUG) {
            printMessage(module, value, DEBUG)
        }
    }

    fun exception(value: Any) {
        if (testMode) {
            return
        }
        val space = "-".repeat(value.toString().length + 2)
        val string = "${ANSI.getBackgroundByCode(160)}${ANSI.HIGH_INTENSITY} +$space+ ${ANSI.SANE}"
        println("\n $string")
        println(" ${ANSI.getBackgroundByCode(160)}${ANSI.WHITE}${ANSI.HIGH_INTENSITY} | $value | ${ANSI.SANE}")
        println(" $string\n")
    }

    private fun printMessage(_module: String, _message: Any?, level: Int) {
        if (testMode) {
            return
        }
        var message = _message
        var module = _module
        if (message == null) {
            message = "No Information :´("
        }
        message = if (message is Array<*>) {
            buildStringFromArray((message as Array<*>?) !!)
        } else {
            message.toString()
        }
        if (module == "") {
            val stackTrace = Thread.currentThread().stackTrace
            val traceElement: StackTraceElement
            traceElement = if (stackTrace.size > 5) {
                stackTrace[4]
            } else {
                stackTrace[3]
            }
            module = traceElement.className + ":" + traceElement.lineNumber
        }
        println(buildLevel(level, true) + prepareModule(module) + message)
    }

    private fun prepareModule(module: String): String {
        return ANSI.HIGH_INTENSITY + " $module  >  " + ANSI.SANE
    }

    private fun buildStringFromArray(list: Array<*>): String {
        val builder = StringBuilder()
        for ((i, item) in list.withIndex()) {
            if (i != 0) {
                builder.append(", ")
            }
            builder.append(item.toString())
        }
        return builder.toString()
    }

    private fun buildLevel(level: Int, useAnsi: Boolean): String {
        val name = getLevelName(level)
        return if (Console.useAnsi && useAnsi) {
            ANSI.HIGH_INTENSITY + getColor(level) + name + ANSI.SANE
        } else name
    }

    private fun getLevelName(level: Int): String {
        return when (level) {
            WARN -> {
                "[WARNING] "
            }
            LOG -> {
                "[NOTICE]  "
            }
            ERROR -> {
                "[CRITICAL]"
            }
            DEBUG -> {
                "[DEBUG]   "
            }
            else -> ""
        }
    }

    private fun getColor(level: Int): String {
        return when (level) {
            WARN -> {
                ANSI.getByCode(11)
            }
            LOG -> {
                ANSI.getByCode(6)
            }
            ERROR -> {
                ANSI.getByCode(160)
            }
            DEBUG -> {
                ANSI.getByCode(208)
            }
            else -> ""
        }
    }

    //little wrapper to make sure no logging format is on it!
    fun raw(message: String) {
        if (testMode) {
            return
        }
        println(message)
    }
}
