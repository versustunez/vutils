package dev.vstz.utils.out

import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

object HashGenerator {
    @Throws(NoSuchAlgorithmException::class)
    fun generateMD2(input: String): String {
        return generateString(input, "MD2", 32)
    }

    @Throws(NoSuchAlgorithmException::class)
    fun generateMD5(input: String): String {
        return generateString(input, "MD5", 32)
    }

    @Throws(NoSuchAlgorithmException::class)
    fun generateSHA1(input: String): String {
        return generateString(input, "SHA-1", 40)
    }

    @Throws(NoSuchAlgorithmException::class)
    fun generateSHA256(input: String): String {
        return generateString(input, "SHA-256", 64)
    }

    @Throws(NoSuchAlgorithmException::class)
    fun generateSHA512(input: String): String {
        return generateString(input, "SHA-512", 128)
    }

    @Throws(NoSuchAlgorithmException::class)
    fun generateSHA384(input: String): String {
        return generateString(input, "SHA-384", 96)
    }

    @Throws(NoSuchAlgorithmException::class)
    private fun generateString(input: String, algorithm: String, minLength: Int): String {
        val messageDigest = MessageDigest.getInstance(algorithm)
        val bytes = messageDigest.digest(input.toByteArray())
        val integer = BigInteger(1, bytes)
        var result = integer.toString(16)
        while (result.length < minLength) {
            result = "0$result"
        }
        return result
    }

    fun getCacheKey(key : String): String {
        return try {
            "cache_${generateMD5(key)}"
        } catch (e: Exception) {
            key
        }
    }
}
