package dev.vstz.utils.out


object ANSI {
    val SANE = "\u001B[0m"

    val HIGH_INTENSITY = "\u001B[1m"
    val LOW_INTENSITY = "\u001B[2m"

    val ITALIC = "\u001B[3m"
    val UNDERLINE = "\u001B[4m"
    val BLINK = "\u001B[5m"
    val RAPID_BLINK = "\u001B[6m"
    val REVERSE_VIDEO = "\u001B[7m"
    val INVISIBLE_TEXT = "\u001B[8m"

    val BLACK = "\u001B[30m"
    val RED = "\u001B[31m"
    val GREEN = "\u001B[32m"
    val YELLOW = "\u001B[33m"
    val BLUE = "\u001B[34m"
    val MAGENTA = "\u001B[35m"
    val CYAN = "\u001B[36m"
    val WHITE = "\u001B[37m"

    val BRIGHT_BLACK = "\u001B[30;1m"
    val BRIGHT_RED = "\u001B[31;1m"
    val BRIGHT_GREEN = "\u001B[32;1m"
    val BRIGHT_YELLOW = "\u001B[33;1m"
    val BRIGHT_BLUE = "\u001B[34;1m"
    val BRIGHT_MAGENTA = "\u001B[35;1m"
    val BRIGHT_CYAN = "\u001B[36;1m"
    val BRIGHT_WHITE = "\u001B[37;1m"

    val BACKGROUND_BLACK = "\u001B[40m"
    val BACKGROUND_RED = "\u001B[41m"
    val BACKGROUND_GREEN = "\u001B[42m"
    val BACKGROUND_YELLOW = "\u001B[43m"
    val BACKGROUND_BLUE = "\u001B[44m"
    val BACKGROUND_MAGENTA = "\u001B[45m"
    val BACKGROUND_CYAN = "\u001B[46m"
    val BACKGROUND_WHITE = "\u001B[47m"

    fun getByCode(code : Int) : String {
        return "\u001b[38;5;${code}m"
    }
    fun getBackgroundByCode(code : Int) : String {
        return "\u001b[48;5;${code}m"
    }
}
